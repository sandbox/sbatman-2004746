(function ($) {

  Drupal.behaviors.modalCta = {
    attach: function (context, settings) {

      dialogTitle = Drupal.t(settings.modal_cta.title);
      dialogCloseMsg = Drupal.t(settings.modal_cta.close_message);
      dialogActionLabel = Drupal.t(settings.modal_cta.action_label);
      dialogActionURL = settings.modal_cta.action_url;

      $("#modal-cta").dialog({
        resizable: false,
        height: settings.modal_cta.height,
        width: settings.modal_cta.width,
        modal: true,
        buttons: [
          {
            text: dialogActionLabel,
            click: function () {
              window.location.href = dialogActionURL;
            }
          },
          {
            text: dialogCloseMsg,
            click: function () {
              $(this).dialog("close");
            }
          }
        ]
      });

    }
  };
})(jQuery);
