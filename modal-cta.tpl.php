<?php
/**
 * @file
 * A basic template for modal call to action window
 *
 * Available variables:
 *  - 'title'
 *  - 'message'
 *  - 'action'
 *  - 'close_message'
 */
?>
<div id="modal-cta" title="<?php print $title?>">
  <p><?php print $message ?></p>
</div>
