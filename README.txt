USAGE
----------------------
The module works in a way similar to the core drupal_set_message().
simply invoking:

modal_cta_set_message(
  "one question",
  "want to see a great module ?",
  array(
    'path' => "http://drupal.org/sandbox/sbatman/2004746",
    "label" => "YES"
  ),
  "close");

will show a call to action within a modal window.

In short:
- invoke modal_cta_set_message() when the display of the call for action modal
  window is triggered
  (e.g. when a node is created, within an hook_node_insert() implementation)
- the modal window will be added to the $page render array rendered during the
  next page request

You can trigger more than one modal window display but, of course, just one
modal window at time will be rendered in each page request (FIFO policy
applied).
